import serial
import time
from aioconsole import ainput

esp32 = serial.Serial('/dev/ttyUSB0', 115200)
print("Connected")

def setValue(index, value):
    index = (index).to_bytes(1, "big")
    value = (value).to_bytes(4, "little", signed=True)
    esp32.write(index + value)
