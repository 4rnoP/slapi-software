endpoint = 'ipc:///tmp/slapi.sock'

printer = \
    {
        'lcd_resolution':
            {
                'x': 2560,
                'y': 1440
            },
        'z_axis':
            {
                # Max height of a 3D Print, in millimeters
                'max_height': 200,
                # Number of steps required to move the build platform, by mm; should be the result of (steps*micro-steps / lead of the screw)
                'steps_by_mm': 200 * 32 / 2,
                # Max motor speed, in mm/sec
                'motor_speed': 10,
                # Max motor acceleration, in mm/sec²
                'motor_accel': 10
            }
    }
