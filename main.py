import zerorpc
import io
import zipfile
import configparser
import motor

import config

class RPC:
    def __init__(self, config):
        self._power = False
        self._calibrate = False
        self._pause = None
        self._printing = False
        self._file = None
        self._file_details = None
        self._z_position = None
        self._config = config
        self._extra_properties = {}

    def state(self):
        """
        Return the state of the printer
        :return:
        {
            "power": self._power,
            "printing": self._printing,
            "calibrated": self._calibrate,
            "file_loaded": bool(self._file_details),
            "config": self.get_config(),

        }
        """
        return {"power": self._power,
                "printing": self._printing,
                "calibrated": self._calibrate,
                "file_loaded": bool(self._file_details),
                "z_position": self._z_position,
                "config": self.get_config()
                }

    def config_get(self):
        """
        Return current config
        :return:
        {
            res_x: <int>,
            res_y: <int>,
            z_speed: <int>,
            z_accel: <int>
        }
        """
        return self._config

    def config_set(self, fields):
        """
        Set some parameters for the printer
        :param fields: A dict containing fields to update, to see available fields, check `get_config()`
        :return: None
        """
        self._config.update(fields)

    def poweron(self, force=False):
        """
        Power on the printer
        :param force: force power on
        :return:
        """
        if self._power is True:
            raise Exception('Already powered on')
        self._power = True

    def poweroff(self, force=False):
        """
        Power of the printer
        :param force: Force poweroff, regardless of the printer state
        :return:
        """
        if force or not self._printing:
            self._power = False
            self._printing = False
        else:
            raise Exception('Currently in a print')

    def load_file(self, file: bytes):
        """
        Load a .sl1 file in memory
        :param file: .sl1 file as a binary object
        """
        if self._printing:
            raise Exception('Currently in a print')
        if self._file:
            self.file_unload()
        file = io.BytesIO(file)
        self._file = zipfile.ZipFile(file)
        self._file_details = self.file_details()
        return self._file_details

    def file_unload(self):
        """
        Unload loaded file
        """
        if self._printing:
            raise Exception('Currently in a print')
        elif not self._file:
            raise Exception('No file loaded')
        self._file = None
        self._file_details = None

    def file_details(self, file: bytes = None):
        """
        Return either details from the loaded file or from a provided file, if specified
        :param file: (Optional) File to scan for details
        :return: <dict>
        """
        if file:
            file = io.BytesIO(file)
            file = zipfile.ZipFile(file)
        else:
            file = self._file
        config = f'[config.ini]\n' \
                 f'{file.read("config.ini").decode()}\n' \
                 f'[prusaslicer.ini]\n' \
                 f'{file.read("prusaslicer.ini").decode()}'
        config_parsed = configparser.ConfigParser()
        config_parsed.read_string(config)
        config_dict = {}
        print(config)
        for k in config_parsed.sections():
            print(k)
            config_dict[k] = {}
            for i, j in config_parsed.items(k):
                config_dict[k][i] = j
        return config_dict

    def start_print(self):
        """
        Start printing the selected file
        """
        if self._printing:
            raise Exception('Currently in a print')
        elif not self._power:
            raise Exception('Not powered')
        elif not self._calibrate:
            raise Exception('Not calibrated')
        elif not self._file:
            raise Exception('No file loaded')
        self._printing = True
        self._pause = False
        # Start printing process

    def pause_print(self):
        """
        Finish layer and pause print
        """
        if not self._printing:
            raise Exception('Not printing')
        if self._pause:
            raise Exception('Already paused')
        self._pause = True

    def resume_print(self):
        """
        Resume print
        """
        if not self._printing:
            raise Exception('Not printing')
        if not self._pause:
            raise Exception('Not paused')
        self._pause = False

    def cancel_print(self):
        """
        Cancel the current print
        """
        if not self._printing:
            raise Exception('Not printing')
        self._printing = False
        self._pause = None
        # Cancel current print

    def enter_calibration(self):
        """
        Enter in calibration process, make possible to move platform beyond minimal values
        """
        if not self._power:
            raise Exception('Not powered')
        elif self._printing:
            raise Exception('Currently in a print')
        self._calibrate = True

    def z_move(self, value, mode=0):
        """
        Move the build platform up or down
        :param value: Target position
        :param mode: Can either be 0 for relative value, or 1 for absolute value
        """
        if not self._calibrate:
            raise Exception('Not calibrating')

    def finish_calibration(self, offset):
        """
        Set current position (offset by param `offset`) to the platform origin.
        Only works when in calibration process.
        :param offset: Offset of the origin regarding the current position
        """
        if not self._calibrate:
            raise Exception('Not calibrating')

    def extra_property_get(self, id):
        return self._extra_properties[id]

    def extra_property_set(self, id, data):
        self._extra_properties[id] = data


# rpc = RPC(config.printer)
# s = zerorpc.Server(rpc)
# s.bind(config.endpoint)
# s.run()
